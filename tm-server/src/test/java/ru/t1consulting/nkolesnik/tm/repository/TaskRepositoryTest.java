package ru.t1consulting.nkolesnik.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.*;


public class TaskRepositoryTest {

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @Nullable
    private static final String NULL_TASK_ID = null;

    @Nullable
    private static final Integer NULL_TASK_INDEX = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @NotNull
    private static final String EMPTY_TASK_ID = "";

    @NotNull
    private static final String EMPTY_USER_ID = "";

    @Nullable
    private static final Task NULL_TASK = null;

    @Nullable
    private static final Sort NULL_SORT = null;

    private static final long REPOSITORY_SIZE = 1000L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String taskId = UUID.randomUUID().toString();

    @NotNull
    private final String projectId = UUID.randomUUID().toString();

    @NotNull
    private List<Task> tasks;

    @NotNull
    private Task task;

    @Before
    public void setup() {
        task = createOneTask();
        tasks = createManyTasks();
    }

    @After
    public void cleanup() {
        repository.clear();
    }

    @Test
    public void add() {
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.add(task);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void addAll() {
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void set() {
        repository.set(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void existById() {
        repository.add(tasks);
        @NotNull final String id = repository.findByIndex(100).getId();
        Assert.assertTrue(repository.existsById(id));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
    }

    @Test
    public void findAll() {
        Assert.assertNotNull(repository.findAll());
        repository.add(tasks);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll().size();
        Assert.assertEquals(repositorySize, findSize);
    }

    @Test
    public void findAllWithSort() {
        repository.add(tasks);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll(Sort.BY_STATUS).size();
        Assert.assertEquals(repositorySize, findSize);
    }

    @Test
    public void findAllWithComporator() {
        repository.add(tasks);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll(Sort.BY_STATUS.getComparator()).size();
        Assert.assertEquals(repositorySize, findSize);
    }

    @Test
    public void findByIndex() {
        repository.add(task);
        Assert.assertEquals(task, repository.findByIndex(0));
        Assert.assertNull(repository.findByIndex(1));
        Assert.assertNull(repository.findByIndex(-1));
    }


    @Test
    public void findById() {
        repository.add(task);
        Assert.assertEquals(task, repository.findById(taskId));
        Assert.assertNull(repository.findById(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findById(NULL_TASK_ID));
    }

    @Test
    public void remove() {
        repository.add(task);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNotNull(repository.remove(task));
        Assert.assertNull(repository.findById(taskId));
    }

    @Test
    public void removeById() {
        repository.add(task);
        Assert.assertEquals(1, repository.getSize());
        repository.removeById(taskId);
        Assert.assertNull(repository.findById(taskId));
    }

    @Test
    public void removeByIndex() {
        repository.add(task);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNotNull(repository.removeByIndex(0));
        Assert.assertNull(repository.removeByIndex(1));
        Assert.assertNull(repository.findById(taskId));
    }

    @Test
    public void removeAll() {
        repository.add(tasks);
        Assert.assertEquals(tasks.size(), repository.getSize());
        repository.removeAll(tasks);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void clear() {
        repository.add(tasks);
        Assert.assertEquals(tasks.size(), repository.getSize());
        repository.clear();
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void getSize() {
        final long tasksSize = tasks.size();
        repository.add(tasks);
        Assert.assertEquals(tasksSize, repository.getSize());
    }

    @Test
    public void addByUserId() {
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.add(userId, task);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void existByIdByUserId() {
        repository.add(tasks);
        @NotNull final String id = repository.findByIndex(100).getId();
        Assert.assertTrue(repository.existsById(userId, id));
        Assert.assertFalse(repository.existsById(userId, EMPTY_TASK_ID));
    }

    @Test
    public void findAllByUserId() {
        repository.add(tasks);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll(userId).size();
        Assert.assertEquals(repositorySize, findSize);
        Assert.assertNotNull(repository.findAll(UUID.randomUUID().toString()));
        Assert.assertNotNull(repository.findAll(NULL_USER_ID));
    }

    @Test
    public void findAllWithSortByUserId() {
        repository.add(tasks);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll(userId, Sort.BY_STATUS).size();
        Assert.assertNotNull(repository.findAll(NULL_USER_ID));
        Assert.assertNotNull(repository.findAll(userId, NULL_SORT));
        Assert.assertEquals(repositorySize, findSize);
        Assert.assertNotNull(repository.findAll(UUID.randomUUID().toString()));
    }

    @Test
    public void findAllWithComparatorByUserId() {
        repository.add(tasks);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll(userId, Sort.BY_STATUS.getComparator()).size();
        Assert.assertNotNull(repository.findAll(NULL_USER_ID));
        Assert.assertNotNull(repository.findAll(userId, NULL_SORT));
        Assert.assertEquals(repositorySize, findSize);
        Assert.assertNotNull(repository.findAll(UUID.randomUUID().toString()));
    }

    @Test
    public void findByIndexByUserId() {
        repository.add(task);
        Assert.assertEquals(task, repository.findByIndex(userId, 0));
        Assert.assertNull(repository.findByIndex(NULL_USER_ID, 1));
        Assert.assertNull(repository.findByIndex(EMPTY_USER_ID, 1));
        Assert.assertNull(repository.findByIndex(userId, NULL_TASK_INDEX));
        Assert.assertNull(repository.findByIndex(userId, 1));
        Assert.assertNull(repository.findByIndex(UUID.randomUUID().toString(), 0));
    }

    @Test
    public void findByIdByUserId() {
        repository.add(task);
        Assert.assertEquals(task, repository.findById(userId, task.getId()));
        Assert.assertNull(repository.findById(NULL_USER_ID, NULL_TASK_ID));
        Assert.assertNull(repository.findById(userId, NULL_TASK_ID));
        Assert.assertNull(repository.findById(EMPTY_USER_ID, task.getId()));
        Assert.assertNull(repository.findById(EMPTY_USER_ID, EMPTY_TASK_ID));
        Assert.assertNull(repository.findById(userId, UUID.randomUUID().toString()));
        Assert.assertNull(repository.findById(UUID.randomUUID().toString(), task.getId()));
        Assert.assertNull(repository.findById(NULL_USER_ID, task.getId()));
    }

    @Test
    public void removeByUserId() {
        repository.add(task);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNull(repository.remove(NULL_USER_ID, task));
        Assert.assertNull(repository.remove(EMPTY_USER_ID, task));
        Assert.assertNull(repository.remove(userId, NULL_TASK));
        Assert.assertNotNull(repository.remove(userId, task));
        Assert.assertNull(repository.findById(userId, task.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        repository.add(task);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNull(repository.removeById(NULL_USER_ID, task.getId()));
        Assert.assertNull(repository.removeById(EMPTY_USER_ID, task.getId()));
        Assert.assertNull(repository.removeById(userId, NULL_TASK_ID));
        Assert.assertNull(repository.removeById(userId, EMPTY_TASK_ID));
        Assert.assertNotNull(repository.removeById(userId, task.getId()));
        Assert.assertNull(repository.findById(userId, task.getId()));
    }

    @Test
    public void removeByIndexByUserId() {
        repository.add(task);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNull(repository.removeByIndex(NULL_USER_ID, 0));
        Assert.assertNull(repository.removeByIndex(EMPTY_USER_ID, 0));
        Assert.assertNull(repository.removeByIndex(userId, NULL_TASK_INDEX));
        Assert.assertNull(repository.removeByIndex(userId, -1));
        Assert.assertNull(repository.removeByIndex(userId, 1));
        Assert.assertNotNull(repository.removeByIndex(task.getUserId(), 0));
        Assert.assertNull(repository.findById(userId, task.getId()));
    }

    @Test
    public void clearByUserId() {
        repository.add(tasks);
        Assert.assertEquals(tasks.size(), repository.getSize());
        repository.clear(userId);
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void getSizeByUserId() {
        repository.add(userId, task);
        Assert.assertEquals(1, repository.getSize(userId));
        repository.removeByIndex(userId, 0);
        Assert.assertEquals(0, repository.getSize(userId));
    }

    @Test
    public void createWithUserIdAndTaskName() {
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.create(userId, TASK_NAME_PREFIX);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void createWithUserIdAndTaskNameAndTaskDescription() {
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.create(userId, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void findAllByProjectId() {
        repository.add(userId, task);
        for (Task task : tasks)
            repository.add(userId, task);
        @NotNull final List<Task> listOfOneTask = new ArrayList<Task>();
        listOfOneTask.add(task);
        @NotNull final List<Task> taskList = repository.findAllByProjectId(userId, projectId);
        Assert.assertEquals(taskList, listOfOneTask);
        Assert.assertNotNull(repository.findAllByProjectId(userId, UUID.randomUUID().toString()));
        @NotNull final Collection<Task> emptyList = Collections.emptyList();
        Assert.assertEquals(emptyList, repository.findAllByProjectId(userId, UUID.randomUUID().toString()));
    }

    @NotNull
    private Task createOneTask() {
        @NotNull final Task task = new Task();
        task.setId(taskId);
        task.setUserId(userId);
        task.setName(TASK_NAME_PREFIX);
        task.setDescription(TASK_DESCRIPTION_PREFIX);
        task.setStatus(Status.IN_PROGRESS);
        task.setProjectId(projectId);
        return task;
    }

    @NotNull
    private List<Task> createManyTasks() {
        @NotNull final List<Task> tasks = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Task task = new Task();
            task.setName(TASK_NAME_PREFIX + i);
            task.setDescription(TASK_DESCRIPTION_PREFIX + i);
            task.setUserId(userId);
            tasks.add(task);
        }
        return tasks;
    }

}