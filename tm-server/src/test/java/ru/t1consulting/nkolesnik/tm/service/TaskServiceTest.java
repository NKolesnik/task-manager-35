package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskService;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ModelNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

public class TaskServiceTest {

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @Nullable
    private static final String NULL_TASK_NAME = null;

    @Nullable
    private static final String NULL_TASK_DESCRIPTION = null;

    @Nullable
    private static final Status NULL_STATUS = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @Nullable
    private static final Integer NULL_TASK_INDEX = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @NotNull
    private static final String EMPTY_TASK_ID = "";

    @NotNull
    private static final String EMPTY_USER_ID = "";

    @Nullable
    private static final Task NULL_TASK = null;

    private static final long REPOSITORY_SIZE = 1000L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectId = UUID.randomUUID().toString();

    @NotNull
    private final String taskId = UUID.randomUUID().toString();

    @NotNull
    private List<Task> tasks;

    @NotNull
    private Task task;

    @Before
    public void setup() {
        task = createOneTask();
        tasks = createManyTasks();
    }

    @After
    public void cleanup() {
        repository.clear();
    }

    @Test
    public void add() {
        Assert.assertThrows(ModelNotFoundException.class, () -> service.add(NULL_TASK));
        service.add(task);
        Assert.assertEquals(1, service.getSize());
        service.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE + 1, service.getSize());
    }

    @Test
    public void set() {
        service.set(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void existById() {
        service.add(task);
        Assert.assertFalse(service.existsById(NULL_TASK_ID));
        Assert.assertFalse(service.existsById(EMPTY_TASK_ID));
        Assert.assertTrue(service.existsById(taskId));
    }

    @Test
    public void findAll() {
        service.add(tasks);
        Assert.assertEquals(tasks, service.findAll());
    }

    @Test
    public void findById() {
        service.add(task);
        service.add(tasks);
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(NULL_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(EMPTY_TASK_ID));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.findById(UUID.randomUUID().toString()));
        Assert.assertEquals(task, service.findById(taskId));
    }

    @Test
    public void findByIndex() {
        service.add(task);
        service.add(tasks);
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(NULL_TASK_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(-1));
        Assert.assertEquals(task, service.findByIndex(0));
    }

    @Test
    public void remove() {
        service.add(task);
        Assert.assertThrows(ModelNotFoundException.class, () -> service.remove(NULL_TASK));
        Assert.assertEquals(task, service.remove(task));
    }

    @Test
    public void removeById() {
        service.add(task);
        service.add(tasks);
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(NULL_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(EMPTY_TASK_ID));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.removeById(UUID.randomUUID().toString()));
        Assert.assertEquals(task, service.removeById(taskId));
    }

    @Test
    public void removeByIndex() {
        service.add(task);
        service.add(tasks);
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(NULL_TASK_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(-1));
        Assert.assertEquals(task, service.removeByIndex(0));
    }

    @Test
    public void removeAll() {
        service.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, service.getSize());
        service.removeAll(tasks);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void clear() {
        service.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, service.getSize());
        service.clear();
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void getSize() {
        service.add(tasks);
        Assert.assertEquals(REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void addWithUserId() {
        service.add(userId, task);
        Assert.assertEquals(1, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(NULL_USER_ID, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(EMPTY_USER_ID, task));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.add(userId, NULL_TASK));
    }

    @Test
    public void existByIdWithUserId() {
        service.add(userId, task);
        Assert.assertTrue(service.existsById(userId, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(NULL_USER_ID, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(EMPTY_USER_ID, taskId));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, NULL_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, EMPTY_TASK_ID));
    }

    @Test
    public void findAllWithUserId() {
        for (Task task : tasks)
            service.add(userId, task);
        service.add(userId, task);
        Assert.assertEquals(REPOSITORY_SIZE + 1, service.findAll(userId).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(EMPTY_USER_ID));
    }


    @Test
    public void findByIdWithUserId() {
        for (Task task : tasks)
            service.add(userId, task);
        service.add(userId, task);
        Assert.assertEquals(task, service.findById(userId, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findById(NULL_USER_ID, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findById(EMPTY_USER_ID, taskId));
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(userId, NULL_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(userId, EMPTY_TASK_ID));
    }

    @Test
    public void findByIndexWithUserId() {
        service.add(userId, task);
        for (Task task : tasks)
            service.add(userId, task);
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(userId, NULL_TASK_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(userId, 10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(userId, -1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findByIndex(NULL_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findByIndex(EMPTY_USER_ID, 0));
        Assert.assertEquals(task, service.findByIndex(userId, 0));
    }

    @Test
    public void removeWithUserId() {
        service.add(userId, task);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(NULL_USER_ID, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(EMPTY_USER_ID, task));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.remove(userId, NULL_TASK));
        Assert.assertEquals(task, service.remove(userId, task));
    }

    @Test
    public void removeByIdWithUserId() {
        service.add(userId, task);
        for (Task task : tasks)
            service.add(userId, task);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(NULL_USER_ID, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(EMPTY_USER_ID, taskId));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, NULL_TASK_ID));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, EMPTY_TASK_ID));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.removeById(userId, UUID.randomUUID().toString()));
        Assert.assertEquals(task, service.removeById(userId, taskId));
    }

    @Test
    public void removeByIndexWithUserId() {
        service.add(task);
        for (Task task : tasks)
            service.add(userId, task);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(NULL_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(userId, NULL_TASK_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(-1));
        Assert.assertEquals(task, service.removeByIndex(userId, 0));
    }

    @Test
    public void clearWithUserId() {
        service.add(userId, task);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(EMPTY_USER_ID));
        service.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void getSizeWithUserId() {
        service.add(userId, task);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(EMPTY_USER_ID));
        service.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void create() {
        service.add(userId, task);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> service.create(NULL_USER_ID, TASK_NAME_PREFIX));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, NULL_TASK_NAME));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, TASK_NAME_PREFIX, NULL_TASK_DESCRIPTION));
    }

    @Test
    public void updateById() {
        service.add(userId, task);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> service.updateById(NULL_USER_ID, taskId, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.updateById(userId, NULL_TASK_ID, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userId, taskId, NULL_TASK_NAME, TASK_DESCRIPTION_PREFIX));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateById(userId, taskId, TASK_NAME_PREFIX, NULL_TASK_DESCRIPTION));
    }

    @Test
    public void updateByIndex() {
        service.add(userId, task);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> service.updateByIndex(NULL_USER_ID, 0, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.updateByIndex(userId, NULL_TASK_INDEX, TASK_NAME_PREFIX, TASK_DESCRIPTION_PREFIX));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(userId, 0, NULL_TASK_NAME, TASK_DESCRIPTION_PREFIX));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateByIndex(userId, 0, TASK_NAME_PREFIX, NULL_TASK_DESCRIPTION));
    }

    @Test
    public void changeStatusById() {
        service.add(userId, task);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> service.changeTaskStatusById(NULL_USER_ID, taskId, Status.NOT_STARTED));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.changeTaskStatusById(userId, NULL_TASK_ID, Status.NOT_STARTED));
        Assert.assertThrows(StatusNotFoundException.class, () -> service.changeTaskStatusById(userId, taskId, NULL_STATUS));
    }

    @Test
    public void changeStatusByIndex() {
        service.add(userId, task);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> service.changeTaskStatusByIndex(NULL_USER_ID, 0, Status.IN_PROGRESS));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeTaskStatusByIndex(userId, NULL_TASK_INDEX, Status.IN_PROGRESS));
        Assert.assertThrows(StatusNotFoundException.class, () -> service.changeTaskStatusByIndex(userId, 0, NULL_STATUS));
    }

    @Test
    public void findAllByProjectId() {
        for (Task task : tasks)
            service.add(userId, task);
        Assert.assertThrows(UserNotFoundException.class, () -> service.findAllByProjectId(NULL_USER_ID, taskId));
        Assert.assertEquals(REPOSITORY_SIZE, service.findAllByProjectId(userId, projectId).size());
    }

    @NotNull
    private Task createOneTask() {
        @NotNull final Task task = new Task();
        task.setId(taskId);
        task.setUserId(userId);
        task.setName(TASK_NAME_PREFIX);
        task.setDescription(TASK_DESCRIPTION_PREFIX);
        task.setStatus(Status.IN_PROGRESS);
        task.setProjectId(projectId);
        return task;
    }

    @NotNull
    private List<Task> createManyTasks() {
        @NotNull final List<Task> tasks = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Task task = new Task();
            task.setName(TASK_NAME_PREFIX + i);
            task.setDescription(TASK_DESCRIPTION_PREFIX + i);
            task.setUserId(userId);
            task.setProjectId(projectId);
            tasks.add(task);
        }
        return tasks;
    }

}
