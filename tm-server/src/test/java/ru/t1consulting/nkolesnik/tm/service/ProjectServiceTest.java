package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectService;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ModelNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.repository.ProjectRepository;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

public class ProjectServiceTest {

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "TEST_PROJECT_DESCRIPTION";

    @Nullable
    private static final String NULL_PROJECT_NAME = null;

    @Nullable
    private static final String NULL_PROJECT_DESCRIPTION = null;

    @Nullable
    private static final Status NULL_STATUS = null;

    @Nullable
    private static final String NULL_PROJECT_ID = null;

    @Nullable
    private static final Integer NULL_PROJECT_INDEX = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @NotNull
    private static final String EMPTY_PROJECT_ID = "";

    @NotNull
    private static final String EMPTY_USER_ID = "";

    @Nullable
    private static final Project NULL_PROJECT = null;

    private static final long REPOSITORY_SIZE = 1000L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final IProjectService service = new ProjectService(repository);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectId = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projects;

    @NotNull
    private Project project;

    @Before
    public void setup() {
        project = createOneProject();
        projects = createManyProjects();
    }

    @After
    public void cleanup() {
        repository.clear();
    }

    @Test
    public void add() {
        Assert.assertThrows(ModelNotFoundException.class, () -> service.add(NULL_PROJECT));
        service.add(project);
        Assert.assertEquals(1, service.getSize());
        service.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE + 1, service.getSize());
    }

    @Test
    public void set() {
        service.set(projects);
        Assert.assertEquals(REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void existById() {
        service.add(project);
        Assert.assertFalse(service.existsById(NULL_PROJECT_ID));
        Assert.assertFalse(service.existsById(EMPTY_PROJECT_ID));
        Assert.assertTrue(service.existsById(projectId));
    }

    @Test
    public void findAll() {
        service.add(projects);
        Assert.assertEquals(projects, service.findAll());
    }

    @Test
    public void findById() {
        service.add(project);
        service.add(projects);
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(NULL_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(EMPTY_PROJECT_ID));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.findById(UUID.randomUUID().toString()));
        Assert.assertEquals(project, service.findById(projectId));
    }

    @Test
    public void findByIndex() {
        service.add(project);
        service.add(projects);
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(NULL_PROJECT_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(-1));
        Assert.assertEquals(project, service.findByIndex(0));
    }

    @Test
    public void remove() {
        service.add(project);
        Assert.assertThrows(ModelNotFoundException.class, () -> service.remove(NULL_PROJECT));
        Assert.assertEquals(project, service.remove(project));
    }

    @Test
    public void removeById() {
        service.add(project);
        service.add(projects);
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(NULL_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(EMPTY_PROJECT_ID));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.removeById(UUID.randomUUID().toString()));
        Assert.assertEquals(project, service.removeById(projectId));
    }

    @Test
    public void removeByIndex() {
        service.add(project);
        service.add(projects);
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(NULL_PROJECT_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(-1));
        Assert.assertEquals(project, service.removeByIndex(0));
    }

    @Test
    public void removeAll() {
        service.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE, service.getSize());
        service.removeAll(projects);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void clear() {
        service.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE, service.getSize());
        service.clear();
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void getSize() {
        service.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void addWithUserId() {
        service.add(userId, project);
        Assert.assertEquals(1, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(NULL_USER_ID, project));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(EMPTY_USER_ID, project));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.add(userId, NULL_PROJECT));
    }

    @Test
    public void existByIdWithUserId() {
        service.add(userId, project);
        Assert.assertTrue(service.existsById(userId, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(NULL_USER_ID, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(EMPTY_USER_ID, projectId));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, NULL_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, EMPTY_PROJECT_ID));
    }

    @Test
    public void findAllWithUserId() {
        for (Project project : projects)
            service.add(userId, project);
        service.add(userId, project);
        Assert.assertEquals(REPOSITORY_SIZE + 1, service.findAll(userId).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(EMPTY_USER_ID));
    }


    @Test
    public void findByIdWithUserId() {
        for (Project project : projects)
            service.add(userId, project);
        service.add(userId, project);
        Assert.assertEquals(project, service.findById(userId, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findById(NULL_USER_ID, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findById(EMPTY_USER_ID, projectId));
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(userId, NULL_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(userId, EMPTY_PROJECT_ID));
    }

    @Test
    public void findByIndexWithUserId() {
        service.add(userId, project);
        for (Project project : projects)
            service.add(userId, project);
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(userId, NULL_PROJECT_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(userId, 10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findByIndex(userId, -1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findByIndex(NULL_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findByIndex(EMPTY_USER_ID, 0));
        Assert.assertEquals(project, service.findByIndex(userId, 0));
    }

    @Test
    public void removeWithUserId() {
        service.add(userId, project);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(NULL_USER_ID, project));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(EMPTY_USER_ID, project));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.remove(userId, NULL_PROJECT));
        Assert.assertEquals(project, service.remove(userId, project));
    }

    @Test
    public void removeByIdWithUserId() {
        service.add(userId, project);
        for (Project project : projects)
            service.add(userId, project);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(NULL_USER_ID, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(EMPTY_USER_ID, projectId));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, NULL_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, EMPTY_PROJECT_ID));
        Assert.assertThrows(ModelNotFoundException.class, () -> service.removeById(userId, UUID.randomUUID().toString()));
        Assert.assertEquals(project, service.removeById(userId, projectId));
    }

    @Test
    public void removeByIndexWithUserId() {
        service.add(project);
        for (Project project : projects)
            service.add(userId, project);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(NULL_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(userId, NULL_PROJECT_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(-1));
        Assert.assertEquals(project, service.removeByIndex(userId, 0));
    }

    @Test
    public void clearWithUserId() {
        service.add(userId, project);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(EMPTY_USER_ID));
        service.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void getSizeWithUserId() {
        service.add(userId, project);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(EMPTY_USER_ID));
        service.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void create() {
        service.add(userId, project);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> service.create(NULL_USER_ID, PROJECT_NAME_PREFIX));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, NULL_PROJECT_NAME));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, PROJECT_NAME_PREFIX, NULL_PROJECT_DESCRIPTION));
    }

    @Test
    public void updateById() {
        service.add(userId, project);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> service.updateById(NULL_USER_ID, projectId, PROJECT_NAME_PREFIX, PROJECT_DESCRIPTION_PREFIX));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.updateById(userId, NULL_PROJECT_ID, PROJECT_NAME_PREFIX, PROJECT_DESCRIPTION_PREFIX));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userId, projectId, NULL_PROJECT_NAME, PROJECT_DESCRIPTION_PREFIX));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateById(userId, projectId, PROJECT_NAME_PREFIX, NULL_PROJECT_DESCRIPTION));
    }

    @Test
    public void updateByIndex() {
        service.add(userId, project);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> service.updateByIndex(NULL_USER_ID, 0, PROJECT_NAME_PREFIX, PROJECT_DESCRIPTION_PREFIX));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.updateByIndex(userId, NULL_PROJECT_INDEX, PROJECT_NAME_PREFIX, PROJECT_DESCRIPTION_PREFIX));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(userId, 0, NULL_PROJECT_NAME, PROJECT_DESCRIPTION_PREFIX));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateByIndex(userId, 0, PROJECT_NAME_PREFIX, NULL_PROJECT_DESCRIPTION));
    }

    @Test
    public void changeStatusById() {
        service.add(userId, project);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> service.changeProjectStatusById(NULL_USER_ID, projectId, Status.NOT_STARTED));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.changeProjectStatusById(userId, NULL_PROJECT_ID, Status.NOT_STARTED));
        Assert.assertThrows(StatusNotFoundException.class, () -> service.changeProjectStatusById(userId, projectId, NULL_STATUS));
    }

    @Test
    public void changeStatusByIndex() {
        service.add(userId, project);
        Assert.assertEquals(1, service.getSize(userId));
        Assert.assertThrows(UserNotFoundException.class, () -> service.changeProjectStatusByIndex(NULL_USER_ID, 0, Status.IN_PROGRESS));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeProjectStatusByIndex(userId, NULL_PROJECT_INDEX, Status.IN_PROGRESS));
        Assert.assertThrows(StatusNotFoundException.class, () -> service.changeProjectStatusByIndex(userId, 0, NULL_STATUS));
    }

    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(projectId);
        project.setUserId(userId);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    private List<Project> createManyProjects() {
        @NotNull final List<Project> projects = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Project project = new Project();
            project.setName(PROJECT_NAME_PREFIX + i);
            project.setDescription(PROJECT_DESCRIPTION_PREFIX + i);
            project.setUserId(userId);
            projects.add(project);
        }
        return projects;
    }

}
