package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.IRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IService;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.exception.entity.ModelNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.IdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.IndexIncorrectException;
import ru.t1consulting.nkolesnik.tm.model.AbstractModel;

import java.util.*;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }

    @Override
    @NotNull
    public Collection<M> add(@NotNull Collection<M> models) {

        return repository.add(models);
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull Collection<M> models) {
        return repository.set(models);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull List<M> models = repository.findAll();
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        @Nullable List<M> models = repository.findAll(comparator);
        if (models == null || models.isEmpty()) return Collections.emptyList();
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @Nullable List<M> models = repository.findAll(sort.getComparator());
        if (models == null || models.isEmpty()) return Collections.emptyList();
        return models;
    }

    @NotNull
    @Override
    public M findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        Optional<M> model = Optional.ofNullable(repository.findByIndex(index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @NotNull
    @Override
    public M findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Optional<M> model = Optional.ofNullable(repository.findById(id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @NotNull
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        repository.remove(model);
        return model;
    }

    @NotNull
    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.removeById(id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        @Nullable final M model = repository.removeByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public void removeAll(@Nullable Collection<M> collections) {
        if (collections == null || collections.isEmpty()) return;
        repository.removeAll(collections);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public long getSize() {
        return repository.getSize();
    }

}
