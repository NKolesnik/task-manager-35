package ru.t1consulting.nkolesnik.tm.dto.response.user;

import lombok.NoArgsConstructor;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractUserResponse;

@NoArgsConstructor
public class UserLogoutResponse extends AbstractUserResponse {

}
